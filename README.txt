Tiny Player

Very simple little flash player, that displays a tiny widget for CCK audio fields.

Just enable the module, and audio fields  (that have display set to tinyplayer in the content-type) will be replaced with a 14x14px player.

Flex source is included, if you want to modify it (like make it a different size, or change the icon images.)
Even if you've never done any flex developing, the player is super-simple, so it should be easy enough to figure out how it works.  You can download the free flex compiler (mxmlc) off Adobe's website.

This module requires swfobject_api and audiofield modules.

There is currently no settings UI, but I plan to add some settings like "use stop/pause" and allow different images, etc. I think even without it, if you need a quick, custom flash audio player, that toggles between play/pause, and doesn't take up alot of space this is the way to go.