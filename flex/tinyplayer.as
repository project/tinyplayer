// simple flex mp3 player
import flash.media.Sound;
import flash.net.URLRequest;

public var playing:Boolean = false;
public var snd:Sound;
public var channel:SoundChannel;
public var pausePosition:int = 0;

// some defaults that can be overidden with the URL params
public var stopOnPause:Boolean = false;
public var mp3:String = "";

private function init():void{
  if (mx.core.Application.application.parameters.mp3){
    mp3 = String(mx.core.Application.application.parameters.mp3);
  }
  if (mx.core.Application.application.parameters.stopOnPause && mx.core.Application.application.parameters.stopOnPause == "true"){
    stopOnPause = true;
  }
  // mx.controls.Alert.show(mp3);
}

public function togglePlay():void {
  playing = !playing;
  if (playing){
    imgPlay.x = -14;
    sndPlay();
  }else{
    imgPlay.x = 0;
    if (stopOnPause){
      sndStop();
    }else{
      sndPause();
    }
  }
}

public function sndPlay():void {
  if (pausePosition == 0){
    snd = new Sound(new URLRequest(mp3));
  }
  if (pausePosition == -1){
    pausePosition = 0;
  }
  channel = snd.play(pausePosition);
}

public function sndPause():void {
  pausePosition = channel.position;
  channel.stop();
}

public function sndStop():void {
  pausePosition = -1;
  channel.stop();
}


