#!/bin/sh
# this will compile on linux / mac

mxmlc -output ../tinyplayer.swf -compiler.optimize tinyplayer.mxml

# this will generate a rudimentary html file if you have swfdump installed.
#swfdump -E tinyplayer.swf |tidy -q -wrap 99999 -asxml -o test.htm 2>/dev/null
